#include <vector>
#include "Haarfeatures.hpp"
#include <utility>
#include "Image.hpp"
#include <string>
#include <iostream>

#ifndef __CASCADECLASSIFIER__
#define __CASCADECLASSIFIER__

/*
    Idea: unline open cv's complicated implementation of hidden haar classifiers and haar classifier differences
    We will have 2 instances 1 Original Cascade and 1 Iterating cascade 

    Iterating cascade will be the one changing (given the scale factor and the original cascade)
    WeakClassifier is evaluated ONLY after the features (and coresponding weights) have been scaled

    We will scale the searching window NOT the image so that we can use the same integral images

*/

/*
 Since scaling the search window will result in a need to modify the cascade classifier, and hence the need for iterating 
 classifier we will implement all methods as part of an interface of existing classes but only to be used on a copy constructed
 cascadeclassifier class

*/
class CascadeClassifier;

class WeakClassifier
{
private:



public:
	WeakClassifier();
	WeakClassifier(const WeakClassifier& obj);
	~WeakClassifier();
	double eval(WeakClassifier *weak_iter,const int x,const int y,uint32_t **sum,double variance_norm_factor) const;
	void print(std::ostream& ostr) const;

	// TODO MAKE PRIVATE
	bool orientation; // true < , false >
	HaarFeature *ft;
	int feature_index;
	double threshold;

	//https://stackoverflow.com/questions/34895186/what-is-the-meaning-of-values-in-stage-xml-and-cascade-xml-for-opencv-cascade-cl
	double left_leaf_val, right_leaf_val;
};



class StrongClassifier
{
private:
	//std::vector<std::pair<double,WeakClassifier>> feature_cascade;

public: 
	StrongClassifier(const StrongClassifier & obj);
	StrongClassifier();
	~StrongClassifier();
	int eval(StrongClassifier *iter_clas,const int x,const int y,uint32_t **sum,double variance) const;
	void print(std::ostream& ostr) const;
	// TODO MAKE PRIVATE
	std::vector<WeakClassifier*> feature_cascade;
	int count_weak;
	double strong_threshold;
};

class CascadeClassifier
{
private: 
	int stages;
	WindowSize orig_size_training;
	WindowSize current_window_size;
	double scale_acumulated;
	double scale_base;
	std::vector<StrongClassifier*> cascade;
	std::vector<HaarFeature*> features;
	GrayscaleImg* face_image;
	/* 
		Either we scale the image (not good since we would need to compute integral image each time)
	 	Or we scale the cascade classifier parameters. This method alows us to do just that.
	 */
	void scaleClassifierForCascade(CascadeClassifier *iter_clas, uint32_t **sum,uint64_t **sqsum, double scale,WeightedRect &wr);


	//bool exist_copy;
public:
	
	/*
		1st param -
		2nd param -
		3rd param - scale by which we should multiply (scale) the image
		4th param - min number of rectangles (object) hits in an area (this is to avoid false positives)
		5th param - should we search for the biggest object? 
		6th param - min size of window that we search (defaults to training image size)
		7th param - max size of window that we search (defaults to image size)
		8th param - should we output the levels where we rejected (?)
	*/
	std::vector<WeightedRect> detectObjects(std::vector<int>& rejectLevels, std::vector<double>& levelWeights,
                     double scaleFactor, int minNeighbors, bool findBiggestObj,WindowSize &minsize,WindowSize &maxsize,bool outputRejectLevels);

	CascadeClassifier(const CascadeClassifier &obj);
	CascadeClassifier();
	~CascadeClassifier();
	void load(std::string& filepath);
	void print(std::ostream & ostr) const;

	// returns 1 if found object in window otherwise negative number of stage in which classification failed
	int eval(CascadeClassifier *scaled_clas,const int x, const int y,uint32_t **sum,uint64_t **sqsum,WeightedRect &equivalized_window,WindowSize maxSize,int starting_stage=0);
	
	void setImg(GrayscaleImg* img);
	void resetImg();
	//TODO DESTRUCTOR
	
};

std::ostream& operator<<(std::ostream& ostr,const CascadeClassifier& cc);

#endif