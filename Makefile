test_classifier: CascadeClassifier.o Haarfeatures.o main_test.o Image.o
	g++ -Wall -g -std=c++11 -lopencv_core -lopencv_imgproc -lopencv_imgcodecs -lopencv_highgui -o test_classifier CascadeClassifier.o Image.o Haarfeatures.o main_test.o
main_test.o: CascadeClassifier.hpp CascadeClassifier.cpp
	g++ -Wall -g -std=c++11 -c main_test.cpp
CascadeClassifier.o: CascadeClassifier.cpp CascadeClassifier.hpp Image.hpp Image.cpp
	g++ -Wall -g -std=c++11 -c CascadeClassifier.cpp
Haarfeatures.o: Haarfeatures.cpp Haarfeatures.hpp Image.hpp Image.cpp
	g++ -Wall -g -std=c++11 -c Haarfeatures.cpp
Image.o: Image.hpp Image.cpp
	g++ -Wall -g -std=c++11 -c Image.cpp

clean: 
	rm -f *.o test_classifier
