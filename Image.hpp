//#include <opencv2/opencv.hpp>
// Needed to image load
#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__
#include <cstdint>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class GrayscaleImg
{
private:
	uint8_t **im;
	int w,h;
	cv::Mat cvImg;
public:
	GrayscaleImg(uint8_t **img,int width,int height)
	:im(img),w(width),h(height)
	{}

	GrayscaleImg()
	{im=nullptr;}
	uint32_t ** calculateIntegralImg() const;
	uint64_t ** calculateSquareIntegralImg() const;

	void readImageGrayscale(std::string &path);
	void loadMatImage(cv::Mat &img_gray);
	int getImgWidth() const;
	int getImgHeight() const;
};

#endif