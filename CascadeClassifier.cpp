#include "CascadeClassifier.hpp"
#include <string>
#include "rapidxml-1.13/rapidxml.hpp"
#include <fstream>
#include <vector>
#include <iterator>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include "Haarfeatures.hpp"
#include <cmath>

using namespace rapidxml;


// Modified because OPENCV classifier values differently if it is (not)  classified correctly


// TODO : prepareImageForCascade,
std::vector<WeightedRect> CascadeClassifier::detectObjects(std::vector<int>& rejectLevels, std::vector<double>& levelWeights,
                     double scaleFactor, int minNeighbors, bool findBiggestObj,WindowSize &minSize,WindowSize &maxSize,bool outputRejectLevels)
{

	int n_factors=0;
	double factor=1;
	double step;

	/*
		region_of_interest_rect is the region of interest that we are scanning (it needs to be the same size as training size window)
		Or if we scalled our classifier , then it should be the same size as the scalled training window size
	*/
	WeightedRect region_of_interest_rect;
	uint32_t **sum;
	uint64_t **sqsum;
	int startX , startY, endX,endY;
	std::vector<WeightedRect> objects_vector;
	std::vector<WeightedRect> candidates;
	/* fscaleFactor must be larger than 1*/

	int img_w,img_h;
	img_w=face_image->getImgWidth();
	img_h=face_image->getImgHeight();

	scale_base=scaleFactor;

	if( maxSize.height == 0 || maxSize.width == 0 )
    {
        maxSize.height = img_h;
        maxSize.width = img_w;
    }

    if (minSize.height == 0 || minSize.width==0)
    {
    	minSize.height=orig_size_training.height;
    	minSize.width=orig_size_training.width;
    }

    sum=face_image->calculateIntegralImg();
    sqsum=face_image->calculateSquareIntegralImg();

    // Creating an iterating cascade classifier that we can change (original must be saved for iteration steps)
    CascadeClassifier *iter_classifier=new CascadeClassifier(*this);
    /* Why exactly 10 ??!?! Magic constant */
    for(;factor*orig_size_training.width < img_w  && factor*orig_size_training.height < img_h ; factor*=scaleFactor)
    {
    	n_factors++;
    }

    if (findBiggestObj)
    {
    	//We are searching for the largest window that constains an objects (which means we go in reverse by size starting from biggest)
    	scaleFactor=1.0/scaleFactor; 
    	factor *= scaleFactor;
    }
    else
    	factor=1;

     for( ; n_factors > 0; factor *= scaleFactor )
     {
     	n_factors--;

     	std::cout << "Testing for scale: " << factor << " n_factors: " << n_factors << std::endl;
     	WindowSize window;
     	WeightedRect equalized_search_window;
     	window.width=cvRound(orig_size_training.width*factor);
     	window.height=cvRound(orig_size_training.height*factor);
     	startX = 0; 
     	startY = 0;

     	//// Important to do this this way since chances are quality of matching will change if i change the step
     	step=std::max(2., factor); 

     	endX=cvRound((img_w-window.width)/step);
     	endY=cvRound((img_h-window.height)/step);


     	if (window.width < minSize.width || window.height < minSize.height)
     	{
     		if (findBiggestObj)
     			break;

     		// Else we ignore this step since window is less then minSize
     		continue;
     	}

     	if (window.width > maxSize.width || window.height > maxSize.height)
     	{
     		if (!findBiggestObj)
     			break;

     		// Else we ignore this step since window is bigger then maxSize
     		continue;
     	}

     	// TODO
     	scaleClassifierForCascade(iter_classifier,sum,sqsum,factor,equalized_search_window);

     	if (region_of_interest_rect.area()>0)
     	{
     		/*	
				TODO 
     		*/
     	}

     	/*
			This is equivalent to HaarDetectObjects_ScaleCascade_Invoker
     	*/

		for(int iter_y = startY; iter_y < endY; iter_y++ )
        {
        	int y=cvRound(iter_y*step);
        	for (int iter_x=startX; iter_x < endX ;iter_x++)
        	{
        		//std::cout << "iter_x : " << iter_x << " iter_y: " << iter_y << std::endl; 
        		int x=cvRound(iter_x*step);

        		int result=eval(iter_classifier,x,y,sum,sqsum,equalized_search_window,maxSize);
        		if (result>0)
        		{
        			WeightedRect potential_candidate;
        			potential_candidate.offset_x=x;
        			potential_candidate.offset_y=y;
        			potential_candidate.width=window.width;
        			potential_candidate.height=window.height;
        			potential_candidate.weight=factor; // For debuging purposes
        			candidates.push_back(potential_candidate);
        		}
        		//else
        		//	std::cout << "Droped in #" << -result << std::endl;
        	}
        }

		/*
			End of HaarDetectObjects_ScaleCascade_Invoker
		*/

     	// TODO FINISH

     	if (findBiggestObj && !candidates.empty() && region_of_interest_rect.area()==0)
     	{

     	}
     }

     return candidates;
     //return objects_vector;
}

/******************** Load classifier **************************/
void CascadeClassifier::load(std::string& filepath)
{

	//https://stackoverflow.com/questions/34895186/what-is-the-meaning-of-values-in-stage-xml-and-cascade-xml-for-opencv-cascade-cl
	std::cout << "Parsing cascade classifier..." << std::endl;
	xml_document<> doc;
	xml_node<> * root_node;
	xml_node<> * cascade_node;
	xml_node<> * stages_node=nullptr;
	xml_node<> * features_node=nullptr;
	xml_node<> * iter_node=nullptr;
	std::ifstream file_xml (filepath.c_str());
	std::vector<char> buff((std::istreambuf_iterator<char>(file_xml)),std::istreambuf_iterator<char>()); 
	// Default constructor for istreambuf_iterator is end of stream
	buff.push_back('\0');
	doc.parse<0>(&buff[0]);
	root_node = doc.first_node("opencv_storage");
	cascade_node=root_node->first_node();
	while (cascade_node)
	{
		//std::cout << cascade_node->name() << std::endl;

		if (strcmp(cascade_node->name(),"features")==0)
			features_node=cascade_node;
		if (strcmp(cascade_node->name(),"stages")==0)
			stages_node=cascade_node;

		if (strcmp(cascade_node->name(),"featureType")==0)
		{
			if (strcmp(cascade_node->value(),"HAAR")!=0)
			{
				std::cout << "Feature type is not a HAAR type feature! Error" << std::endl;
				return;
			}
		}

		if (strcmp(cascade_node->name(),"stageNum")==0)
		{
			stages=std::stoi(cascade_node->value());
		}
		
		if (strcmp(cascade_node->name(),"height")==0)
		{
			orig_size_training.height=std::stoi(cascade_node->value());
			//std::cout << orig_size_training.height << std::endl;
		}

		if (strcmp(cascade_node->name(),"width")==0)
		{
			orig_size_training.width=std::stoi(cascade_node->value());
			//std::cout << orig_size_training.width << std::endl;
		}

		if (strcmp(cascade_node->name(),"cascade")==0)
		{
			root_node=cascade_node;
			cascade_node=root_node->first_node();
		}
		else
		{
			cascade_node=cascade_node->next_sibling();
		}

	}

	if (features_node==nullptr || stages_node==nullptr)
	{
		std::cout << "Error: parameters for classification or features used in training not given." << std::endl;
		return;
	}

	cascade_node=features_node->first_node();
	while (cascade_node)
	{
		// This is the rects field
		iter_node=cascade_node->first_node(); 
		HaarFeature* hf=new HaarFeature();
		// We need to iterate through all rectangles of this one feature
		for (xml_node<> *tmp_node=iter_node->first_node();tmp_node;tmp_node=tmp_node->next_sibling())
		{
			// TODO  CHECK AND FINISH
			//std::cout << tmp_node->value() << std::endl;
			std::istringstream iss(tmp_node->value());
			//std::cout << tmp_node->value() << std::endl;
			WeightedRect* wr=new WeightedRect();
			if (!(iss>>wr->offset_x))
			{
				std::cout << "Error while parsing rects for feature values" << std::endl;
				return;
			}

			if (!(iss>>wr->offset_y))
			{
				std::cout << "Error while parsing rects for feature values" << std::endl;
				return;
			}

			if (!(iss>>wr->width))
			{
				std::cout << "Error while parsing rects for feature values" << std::endl;
				return;
			}

			if (!(iss>>wr->height))
			{
				std::cout << "Error while parsing rects for feature values" << std::endl;
				return;
			}

			if (!(iss>>wr->weight))
			{
				std::cout << "Error while parsing rects for feature values" << std::endl;
				return;
			}

			hf->rects.push_back(wr);
		}

		features.push_back(hf);

		cascade_node=cascade_node->next_sibling();
	}

	cascade_node=stages_node->first_node();
	while (cascade_node) // iterating by strong classifiers
	{
		// Currently the cascade node is the <_> node in the xml
		StrongClassifier *sc=new StrongClassifier();
		iter_node=cascade_node->first_node();
		for (;iter_node;iter_node=iter_node->next_sibling()) // iterating by strong classifier parameters
		{
			if (strcmp(iter_node->name(),"maxWeakCount")==0)
			{
				sc->count_weak=std::stoi(iter_node->value());
			}
									  
			if (strcmp(iter_node->name(),"stageThreshold")==0)
			{
				sc->strong_threshold=std::stod(iter_node->value());
			}

			if (strcmp(iter_node->name(),"weakClassifiers")==0)
			{
				// Now to fill the weak classifiers in this strong classifier
				xml_node<> *weak_iter=iter_node->first_node();
				//std::cout << weak_iter->value() << std::endl;
				while (weak_iter)
				{
					WeakClassifier *wc=new WeakClassifier();
					xml_node<>* nd_weak_helper=weak_iter->first_node();
					while (nd_weak_helper)
					{
						if (strcmp(nd_weak_helper->name(),"internalNodes")==0)
						{
							//std::cout << nd_weak_helper->value() << std::endl;
							std::istringstream iss(nd_weak_helper->value());
							int dummy;
							if (!(iss>>dummy))
							{
								std::cout <<"Error while parsing weak classifiers" << std::endl;
							}
							if (!(iss>>dummy))
							{
								std::cout <<"Error while parsing weak classifiers" << std::endl;
							}

							// 3rd number is a feature index - its not a dummy variable (though we recicle it)
							if (!(iss>>dummy))
							{
								std::cout <<"Error while parsing weak classifiers" << std::endl;
							}
							wc->feature_index=dummy;
							wc->ft=features[dummy];

							if (!(iss>>wc->threshold))
							{
								std::cout <<"Error while parsing weak classifiers" << std::endl;
							}
						}

						if (strcmp(nd_weak_helper->name(),"leafValues")==0)
						{
							//std::cout << nd_weak_helper->value() << std::endl;
							std::istringstream iss(nd_weak_helper->value());
							if (!(iss>>wc->left_leaf_val))
							{
								std::cout <<"Error while parsing weak classifiers - leaf Vals" << std::endl;
							}

							if (!(iss>>wc->right_leaf_val))
							{
								std::cout <<"Error while parsing weak classifiers - leaf Vals" << std::endl;
							}
						}

						nd_weak_helper=nd_weak_helper->next_sibling();
					}

					sc->feature_cascade.push_back(wc);
					weak_iter=weak_iter->next_sibling();
				}

			}
		}

		cascade.push_back(sc);
		cascade_node=cascade_node->next_sibling();
	}
}
/*********************** End of load classifier********************************/

/***************** CONSTRUCTORS *********************/
CascadeClassifier::CascadeClassifier()
{
	stages=0;
	orig_size_training.width=0;
	orig_size_training.height=0;
	current_window_size.width=0;
	current_window_size.height=0;
	scale_base=1.2;
	scale_acumulated=1;
	face_image=nullptr;
}

CascadeClassifier::CascadeClassifier(const CascadeClassifier &obj)
{
	stages=obj.stages;
	orig_size_training.width=obj.orig_size_training.width;
	orig_size_training.height=obj.orig_size_training.height;

	current_window_size.width=obj.current_window_size.width;
	current_window_size.height=obj.current_window_size.height;
	scale_base=obj.scale_base;
	scale_acumulated=obj.scale_acumulated;

	for (std::vector<HaarFeature*>::const_iterator iter=obj.features.cbegin();iter!=obj.features.cend();iter++)
	{
		HaarFeature *hf=new HaarFeature(*(*iter));
		features.push_back(hf);
	}

	for (std::vector<StrongClassifier*>::const_iterator iter=obj.cascade.cbegin();iter!=obj.cascade.cend();iter++)
	{
		StrongClassifier * sc=new StrongClassifier(*(*iter));
		for (std::vector<WeakClassifier*>::const_iterator iter2=sc->feature_cascade.cbegin();iter2!=sc->feature_cascade.cend();iter2++)
		{
			(*iter2)->ft=features[(*iter2)->feature_index];
		}
		cascade.push_back(sc);
	}

	face_image=obj.face_image;


}

StrongClassifier::StrongClassifier(const StrongClassifier & obj)
{
	count_weak=obj.count_weak;
	strong_threshold=obj.strong_threshold;
	for (std::vector<WeakClassifier*>::const_iterator iter=obj.feature_cascade.cbegin();iter!=obj.feature_cascade.cend();iter++)
	{
		WeakClassifier* wc=new WeakClassifier(*(*iter));
		feature_cascade.push_back(wc);
	}
}

StrongClassifier::StrongClassifier()
{

}

WeakClassifier::WeakClassifier()
{

}

WeakClassifier::WeakClassifier(const WeakClassifier& obj)
{
	orientation=obj.orientation;
	left_leaf_val=obj.left_leaf_val;
	right_leaf_val=obj.right_leaf_val;
	threshold=obj.threshold;
	feature_index=obj.feature_index;
}
/*******************************************************/


/******************** DESTRUCTORS **********************/

CascadeClassifier::~CascadeClassifier()
{
	for (unsigned i=0;i<cascade.size();i++)
		delete cascade[i];
	
	for (unsigned i=0;i<features.size();i++)
		delete features[i];
}

StrongClassifier::~StrongClassifier()
{
	for (unsigned i=0;i<feature_cascade.size();i++)
		delete feature_cascade[i];
}

WeakClassifier::~WeakClassifier()
{
	// Haar feature should be deleted through features vector in CascadeClassifier ( weakclassifier just holds the pointer)
}
/*******************************************************/

/******************** PRINT METHODS *******************/

void CascadeClassifier::print(std::ostream& ostr) const
{

	ostr << "**************** BEGIN OF CASCADECLASSIFIER*************************" << std::endl;
	ostr << "Stages: " << stages << std::endl;
	ostr << "Traning window size (H x W): " << orig_size_training.height <<"x" << orig_size_training.width << std::endl; 
	ostr << "Current window size (H x W): " << current_window_size.height <<"x" << current_window_size.width << std::endl;
	ostr << "Scaling factor: " << scale_base << std::endl;
	ostr << "Scaling acumulated (so far)" << scale_acumulated << std::endl;
	if (face_image!=nullptr)
		ostr << "Img dimensions: " << face_image->getImgHeight() << "x" <<face_image->getImgWidth() << std::endl;
	ostr << "Listing strong classifiers: " << std::endl;
	for (unsigned i=0;i<cascade.size();i++)
	{
		cascade[i]->print(ostr);
	}
	ostr << "Listing features: " << std::endl;
	for (unsigned i=0;i<features.size();i++)
	{
		features[i]->print(ostr);
	}

	ostr << "****************** END OF CASCADECLASSIFIER**************************" << std::endl;
}

void StrongClassifier::print(std::ostream& ostr) const
{
	ostr << "-------------- BEGIN OF STRONG CLASSIFIER -------------------" << std::endl;
	ostr << "Weak classifiers count: " << count_weak << std::endl;
	ostr << "Strong classifier threshold: " << strong_threshold << std::endl;
	ostr << "Listing weak classifiers: " << std::endl;

	for (unsigned i=0;i<feature_cascade.size();i++)
	{
		feature_cascade[i]->print(ostr);
	}

	ostr << "-------------- END OF STRONG CLASSIFIER   -------------------" << std::endl;
}


void WeakClassifier::print(std::ostream& ostr) const
{
	ostr << "************** Begin of weak classifier *******************" << std::endl;
	ostr <<"Index of haar feature: "<< feature_index <<std::endl;
	ostr <<"Weak classifier threshold: " << threshold <<std::endl;
	ostr <<"Left leaf value: " << left_leaf_val <<std::endl;
	ostr <<"Right leaf value: " << right_leaf_val <<std::endl;
	ft->print(ostr);
	ostr << "************** End   of weak classifier *******************" << std::endl;
}


std::ostream& operator<<(std::ostream& ostr,const CascadeClassifier &cc)
{
	cc.print(ostr);
	return ostr;
}

/******************************************************/

/******************* Classifier scaling *********************************/

/*
	TODO CHECK IF icv_border=1 (-2 on width and height on equRect) matters
	Also check if kx,ky can be zero EVER
*/
void CascadeClassifier::scaleClassifierForCascade(CascadeClassifier *iter_clas, uint32_t **sum,uint64_t **sqsum, double scale, WeightedRect &wr)
{
	const int object_window_border=1;


	current_window_size.width=cvRound(orig_size_training.width*scale);
	current_window_size.height=cvRound(orig_size_training.height*scale);
	scale_acumulated=scale;
	wr.offset_x=cvRound(scale)-1;
	wr.offset_y=cvRound(scale)-1;
	wr.width=cvRound((orig_size_training.width-2*object_window_border)*scale);
	wr.height=cvRound((orig_size_training.height-2*object_window_border)*scale);
	wr.weight=1./(wr.width*wr.height); 

	for (unsigned i=0;i<iter_clas->cascade.size();i++) // Iterating through strong classifiers
	{
		for (unsigned j=0;j<iter_clas->cascade[i]->feature_cascade.size();j++) // Iterating through weak classifiers
		{
			/*
				Todo check if kx, ky can be zero 
			*/
			double sum=0,area=0;
			for (unsigned k=0;k<iter_clas->cascade[i]->feature_cascade[j]->ft->rects.size();k++) // Iterating through weighted rectangles of a single haar feature
			{
				WeightedRect *wr_iter=iter_clas->cascade[i]->feature_cascade[j]->ft->rects[k];
				WeightedRect *wr_orig=cascade[i]->feature_cascade[j]->ft->rects[k];


				wr_iter->offset_x=cvRound(wr_orig->offset_x*scale);
				wr_iter->offset_y=cvRound(wr_orig->offset_y*scale);
				wr_iter->width=cvRound(wr_orig->width*scale);
				wr_iter->height=cvRound(wr_orig->height*scale);
				wr_iter->weight=wr_orig->weight*wr.weight;


				// This is dont just because we can have non balanced features (eg feature of original weight 3 is not 1/3 )
				// Usually if k=0  wr_iter->weight can be set to -wr.weight (-1/SCALED_AREA_WITHOUT_BORDERS)
				/*
					Todo check - set wr_iter->weight for k=0 to -wr.weight
				*/
				if (k==0)
				{
					area=wr_iter->width*wr_iter->height;
				}
				else
				{
					sum+=wr_iter->weight * wr_iter->width * wr_iter->height;
				}

			}

			iter_clas->cascade[i]->feature_cascade[j]->ft->rects[0]->weight=-sum/area;



		}
	}
}


/*********************** END Of classifier scalling *********************************/

/********************** Eval functions*************************************************************/
int CascadeClassifier::eval(CascadeClassifier *scaled_clas,const int x, const int y,uint32_t **sum,uint64_t **sqsum,WeightedRect &equivalized_window,WindowSize maxSize,int starting_stage)
{

	double mean,variance_norm_factor,sigma;	

	// the +1 are used for rounding errors
	// eg scale 12 by 1.21 and round it we get 15 
	// scale 24 by 1.21 and round it we get 29     2*15 > 29 so the scaled subrectangle would drop from the scaled rectangle
	if (x<0 || y<0 || (x+ current_window_size.width+1) >= (maxSize.width) || (y+current_window_size.height+1)>=(maxSize.height))
		return -1;

	// p0 + p3 - p1 - p2
	mean=sum[equivalized_window.offset_y][equivalized_window.offset_x] + sum[equivalized_window.offset_y+equivalized_window.height][equivalized_window.offset_x+equivalized_window.width] 
		-sum[equivalized_window.offset_y][equivalized_window.offset_x+equivalized_window.width] - sum[equivalized_window.offset_y+equivalized_window.height][equivalized_window.offset_x];

	// We need to divide by the number of pixels that we have 
	mean=mean*equivalized_window.weight;
	//http://pi-virtualworld.blogspot.rs/2014/01/integral-image-for-mean-and-variance.html
	// We calculate the variance as sum of all pixels (squared) - mean squared
	variance_norm_factor=sqsum[equivalized_window.offset_y][equivalized_window.offset_x] + sqsum[equivalized_window.offset_y+equivalized_window.height][equivalized_window.offset_x+equivalized_window.width] 
		-sqsum[equivalized_window.offset_y][equivalized_window.offset_x+equivalized_window.width] - sqsum[equivalized_window.offset_y+equivalized_window.height][equivalized_window.offset_x];
	// We need to divide the sum squared by the number of pixels so that we can get the single pixel variance
	variance_norm_factor=variance_norm_factor*equivalized_window.weight - mean*mean;
	
	// sigma^2 = variance
	if (variance_norm_factor>=0)
		sigma=std::sqrt(variance_norm_factor);
	else
		sigma=1;

	for (unsigned i=0;i<scaled_clas->cascade.size();i++)
	{	
		if (cascade[i]->eval(scaled_clas->cascade[i],x,y,sum,sigma)<0)
			return -i;
	}

	return 1;
}

int StrongClassifier::eval(StrongClassifier *iter_strong,const int x,const int y,uint32_t **sum,double variance) const
{
	double sum_acc=0;

	for (unsigned i=0;i<iter_strong->feature_cascade.size();i++)
	{
		sum_acc+=feature_cascade[i]->eval(iter_strong->feature_cascade[i],x,y,sum,variance);
	}

	// If the sum of feature evaluators is high that means a given feature is present with high probability 
	// Meaning if our sum is below the threshold we do not have a given feature and we failed to find an object

	if (sum_acc < iter_strong->strong_threshold)
		return -1;
	else
		return 1;
}


double WeakClassifier::eval(WeakClassifier *weak_iter,const int x,const int y,uint32_t **sum,double variance_norm_factor) const
{
	double adapted_treshold=weak_iter->threshold * variance_norm_factor;
	double acum=0;

	for (unsigned i=0;i<weak_iter->ft->rects.size();i++)
	{
		WeightedRect *wr=weak_iter->ft->rects[i];
		
		double tmp=sum[y+wr->offset_y+wr->height-1][x+wr->offset_x+wr->width-1];
		if ((y+wr->offset_y)>0)
			tmp-=sum[y+wr->offset_y-1][x+wr->offset_x+wr->width-1];
		
		if ((x+wr->offset_x)>0)
			tmp-=sum[y+wr->offset_y+wr->height-1][x+wr->offset_x-1];

		if ((y+wr->offset_y)>0 && (x+wr->offset_x)>0)
			tmp+=sum[y+wr->offset_y-1][x+wr->offset_x-1];
		
		acum+=tmp*wr->weight;
		//acum+=(sum[y+wr->offset_y][x+wr->offset_x]+sum[y+wr->offset_y+wr->height-1][x+wr->offset_x+wr->width-1]-sum[y+wr->offset_y+wr->height-1][x+wr->offset_x]-sum[y+wr->offset_y][x+wr->offset_x+wr->width-1])*wr->weight;
	}

	if (acum<adapted_treshold)
		return weak_iter->left_leaf_val;
	else
		return weak_iter->right_leaf_val;
}
/************************* End of eval functions*************************************/

void CascadeClassifier::setImg(GrayscaleImg* img)
{
	face_image=img;
}

void CascadeClassifier::resetImg() 
{
	face_image=nullptr;
}