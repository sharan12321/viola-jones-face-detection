#include "CascadeClassifier.hpp"
#include <string>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "Image.hpp"

int main(int argc, char const *argv[])
{
	CascadeClassifier cclas;
	cv::Mat ImageColor;
	cv::Mat ImageGray;
	GrayscaleImg* img_gray=new GrayscaleImg();
	std::string file_path("../opencv-3.2.0/opencv-3.2.0/data/haarcascades/haarcascade_frontalface_default.xml");
	std::vector<WeightedRect> potential_res;


	if (argc!=2 && argc!=3)
	{
		std::cerr << "Need to input image name in which to search for faces" << std::endl;
		return 1;
	}

	ImageColor=cv::imread(argv[1],cv::IMREAD_COLOR);

	if (!ImageColor.data)
	{
		std::cerr << "Cannot open or find the image file" << std::endl;
		return 1;
	}

	
	//Loading classifier
	cclas.load(file_path);

	cv::cvtColor( ImageColor, ImageGray, CV_BGR2GRAY );
    cv::equalizeHist( ImageGray, ImageGray );

    cv::imshow("Prikaz slike u boji",ImageGray);

	cv::waitKey(0); // keystroke in the window

	// Reading image to our structure 
	img_gray->loadMatImage(ImageGray);

	// Setting the classifier to work with our image
	cclas.setImg(img_gray);

	// Runing classifier
	std::vector<int> reject_lvls;
	std::vector<double> lvl_weights;
	WindowSize minSize;
	WindowSize maxSize;
	minSize.width=0;
	minSize.height=0;
	maxSize.width=0;
	maxSize.height=0;
	potential_res=cclas.detectObjects(reject_lvls,lvl_weights,1.1,0,false,minSize,maxSize,false);

	for (auto iter=potential_res.cbegin();iter!=potential_res.cend();iter++)
	{
		std::cout << (*iter).offset_x << " " << (*iter).offset_y << " " << (*iter).width << " " << (*iter).height << " Scale: " << (*iter).weight << std::endl;
	}

	for( size_t i = 0; i < potential_res.size(); i++ )
  	{
    	cv::Point center( potential_res[i].offset_x + potential_res[i].width*0.5, potential_res[i].offset_y + potential_res[i].height*0.5 );
    	cv::ellipse( ImageColor, center, cv::Size( potential_res[i].width*0.5, potential_res[i].height*0.5), 0, 0, 360, cv::Scalar( 255, 0, 255 ), 4, 8, 0 );
	}


	cv::namedWindow("Prikaz slike u boji", cv::WINDOW_AUTOSIZE);
	cv::imshow("Prikaz slike u boji",ImageColor);

	cv::waitKey(0); // keystroke in the window
	
	//std::cout << cclas << std::endl;
	return 0;
}