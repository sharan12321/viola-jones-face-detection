#include "Image.hpp"
#include <cstdint>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

int GrayscaleImg::getImgWidth() const
{
	return w;
}

int GrayscaleImg::getImgHeight() const
{
	return h;
}

uint32_t** GrayscaleImg::calculateIntegralImg() const
{
	int i,j;
	uint32_t **tmp=new uint32_t*[h];
	for (i=0;i<h;i++)
		tmp[i]=new uint32_t[w];

	tmp[0][0]=static_cast<uint32_t>(im[0][0]);
	for (j=1;j<w;j++)
	{
		tmp[0][j]=static_cast<uint32_t>(im[0][j]);
		tmp[0][j]+=tmp[0][j-1];
	}

	for (i=1;i<h;i++)
	{
		tmp[i][0]=static_cast<uint32_t>(im[i][0]);
		tmp[i][0]+=tmp[i-1][0];
	}

	for (i=1;i<h;i++)
	{
		for (j=1;j<w;j++)
		{
			tmp[i][j]=static_cast<uint32_t>(im[i][j]);
			
			tmp[i][j]+=(tmp[i-1][j]+tmp[i][j-1]-tmp[i-1][j-1]);
		}
	}

	return tmp;
}

uint64_t ** GrayscaleImg::calculateSquareIntegralImg() const
{
	int i,j;
	uint64_t **tmp=new uint64_t*[h];
	for (i=0;i<h;i++)
		tmp[i]=new uint64_t[w];
	tmp[0][0]=static_cast<uint64_t>(im[0][0])*static_cast<uint64_t>(im[0][0]);

	for (j=1;j<w;j++)
	{
		tmp[0][j]=static_cast<uint64_t>(im[0][j])*static_cast<uint64_t>(im[0][j]);
		tmp[0][j]+=tmp[0][j-1];
	}

	for (i=1;i<h;i++)
	{
		tmp[i][0]=static_cast<uint64_t>(im[i][0])*static_cast<uint64_t>(im[i][0]);
		tmp[i][0]+=tmp[i-1][0];
	}

	for (i=1;i<h;i++)
	{
		for (j=1;j<w;j++)
		{
			tmp[i][j]=static_cast<uint64_t>(im[i][j])*static_cast<uint64_t>(im[i][j]);
			
			tmp[i][j]+=(tmp[i-1][j]+tmp[i][j-1]-tmp[i-1][j-1]);
		}
	}
	
	return tmp;
}

void GrayscaleImg::readImageGrayscale(std::string &path)
{
	int i,j;
	unsigned char* data_im;
	cvImg=cv::imread(path.c_str(),cv::IMREAD_GRAYSCALE);
	w=cvImg.cols;
	h=cvImg.rows;
	im=new uint8_t*[h];
	data_im=cvImg.data;
	for (i=0;i<h;i++)
	{
		im[i]=new uint8_t[w];
	}

	for (i=0;i<cvImg.rows;i++)
	{
		for (j=0;j<cvImg.cols;j++)
		{
			im[i][j]=static_cast<uint8_t>(*(data_im+i*cvImg.cols+j));
		}
	}
}

void GrayscaleImg::loadMatImage(cv::Mat& img_gray)
{
	int i,j;
	unsigned char* data_im;
	cvImg=img_gray;
	w=cvImg.cols;
	h=cvImg.rows;
	im=new uint8_t*[h];
	data_im=cvImg.data;
	for (i=0;i<h;i++)
	{
		im[i]=new uint8_t[w];
	}

	for (i=0;i<cvImg.rows;i++)
	{
		for (j=0;j<cvImg.cols;j++)
		{
			im[i][j]=static_cast<uint8_t>(*(data_im+i*cvImg.cols+j));
		}
	}
}