#include "Haarfeatures.hpp"

double HaarFeature::eval(uint32_t ** img,int &x1,int &y1) const
{
	double rct_sum=0;
	std::vector<WeightedRect*>::const_iterator iter=rects.cbegin();

	for (;iter!=rects.cend();iter++)
	{
		WeightedRect* wr=(*iter);
		//rct_sum+=img[y1][x1] Todo get points add them y1+y_offset x1+x_offset is the upper left point of rects feature in a sum img
		rct_sum+=(img[y1+wr->offset_y][x1+wr->offset_x]-img[y1+wr->offset_y][x1+wr->offset_x+wr->width]-img[y1+wr->offset_y+wr->height][x1+wr->offset_x]+img[y1+wr->offset_y+wr->height][x1+wr->offset_x+wr->width])*wr->weight;
	}
	

	return rct_sum;
}

HaarFeature::HaarFeature()
{

}

HaarFeature::HaarFeature(const HaarFeature& obj)
{
	for (std::vector<WeightedRect*>::const_iterator iter=obj.rects.cbegin();iter!=obj.rects.cend();iter++)
	{
		WeightedRect *wr=new WeightedRect();
		wr->width=(*iter)->width;
		wr->height=(*iter)->height;
		wr->weight=(*iter)->weight;
		wr->offset_x=(*iter)->offset_x;
		wr->offset_y=(*iter)->offset_y;
		rects.push_back(wr);
	}
}

HaarFeature::~HaarFeature()
{
	for (unsigned i=0;i<rects.size();i++)
		delete rects[i];
}

void HaarFeature::print(std::ostream& ostr) const
{
	ostr << "++++++++++ Begin of Haar Feature ++++++++++++" << std::endl;
	ostr << "Printing weighted rectangles (x,y,width,height,weight)" << std::endl;
	for (unsigned i=0;i<rects.size();i++)
	{
		rects[i]->print(ostr);
	}
	ostr << "++++++++++ End   of Haar Feature ++++++++++++" << std::endl;
}

unsigned WeightedRect::area() const
{
	return height*width;
}

void WeightedRect::print(std::ostream& ostr) const
{
	ostr << offset_x<< ", " << offset_y << ", " << width <<", " << height << ", " << weight << std::endl;
}

WeightedRect::WeightedRect()
{
	width=0;
	height=0;
	weight=0;
	offset_x=0;
	offset_y=0;
}