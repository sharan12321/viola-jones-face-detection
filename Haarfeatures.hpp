#include "Image.hpp"
#include <vector>
#include <iostream>

#ifndef __HAARFEATURES__
#define __HAARFEATURES__

class WeightedRect
{
public:
	int width;
	int height;
	double weight;
	int offset_x;
	int offset_y;
	unsigned area() const;
	void print(std::ostream &ostr) const;
	WeightedRect();

};

typedef struct 
{
	int width;
	int height;
} WindowSize;

class HaarFeature
{
protected:
	// To know where the HaarFeature rect are in the window we are watching
public: 
	HaarFeature();
	HaarFeature(const HaarFeature& obj);
	~HaarFeature();
	double eval(uint32_t** img,int &x1,int &y1) const;
	void print(std::ostream& ostr) const;
	std::vector<WeightedRect*> rects;
};



#endif