#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


// Compilation
// g++ -lopencv_core -lopencv_highgui main.cpp -o displayimg
int main(int argc, char const *argv[])
{
	std::cout << cv::getBuildInformation() <<std::endl;
	cv::Mat image;
	if (argc!=2 && argc!=3)
	{
		std::cerr << "Need to input image name in which to search for faces" << std::endl;
		return 1;
	}

	image=cv::imread(argv[1],cv::IMREAD_COLOR);

	if (!image.data)
	{
		std::cerr << "Cannot open or find the image file" << std::endl;
		return 1;
	}

	cv::namedWindow("Prikaz slike", cv::WINDOW_AUTOSIZE);
	cv::imshow("Prikaz slike",image);

	cv::waitKey(0); // keystroke in the window

	return 0;

}
